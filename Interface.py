import re
import db

load_data_called = False
start_screen = """
 _   _______ _       _____                                   _____ _     _     
| \ | |  ___| |     |_   _|                          ___    |  _  | |   ( )    
|  \| | |_  | |       | | ___  __ _ _ __ ___  ___   ( _ )   | | | | |__ |/ ___ 
| . ` |  _| | |       | |/ _ \/ _` | '_ ` _ \/ __|  / _ \/\ | | | | '_ \  / __|
| |\  | |   | |____   | |  __| (_| | | | | | \__ \ | (_>  < \ \/' | |_) | \__ \ 
\_| \_\_|   \_____/   \_/\___|\__,_|_| |_| |_|___/  \___/\/  \_/\_|_.__/  |___/
                                                                               """
def main():
    #setup the database
    db.instance.setup()
    # load data flag
    data_loaded = False
    #checks if the db is already loaded
    if (db.instance.isDataReady()):
        data_loaded = True

    # lists of keywords
    team_attributes = ["city", "formed", "starting_qb", "backup_qb", "all_qbs"]
    qb_attributes = ["college", "drafted", "is_starting", "team_name"]

    # startup function
    startup()

    # get user input and split into list of words
    user_input = input("#").lower()
    user_input_words = user_input.split()
    name_of_team_or_qb = re.findall('"([^"]*)"', user_input)

    # adds empty string to list if its empty to avoid index out of bounds
    if (len(name_of_team_or_qb) == 0):
        name_of_team_or_qb.append(" ")

    # input loop
    while user_input != "exit":
        if user_input == "help":
            help()
        elif user_input == "load data":
            if data_loaded:
                print("Data is already loaded, no need to load again")
            else:
                db.instance.loadData()
                data_loaded = True
                print("Data is loaded")

        elif not data_loaded:
            print("load data must be called before querying for data")
        elif len(user_input_words) < 1:
            pass
        else:
            parse(user_input, user_input_words, name_of_team_or_qb, team_attributes, qb_attributes)

        # get user input and split into array of words
        user_input = input("#").lower()
        user_input_words = user_input.split()
        name_of_team_or_qb = re.findall('"([^"]*)"', user_input)

        # adds empty string to list if its empty to avoid index out of bounds
        if (len(name_of_team_or_qb) == 0):
            name_of_team_or_qb.append(" ")

    # close the connection when the program terminates
    db.instance.finish()

# help screen function
# Prints text to the user with info on how to use the interface
def help():
    print("""Commands accepted are in the following format:
              *Note: The name of the qb or the team name must be in double quotes*
                a) *attribute of a team* team *the name of the team*
                b) *attribute of a qb* qb *the name of the qb*
                c) *attribute of a qb* qb team *the name of the team*
              Other Commands:
                a) number of teams
                b) number of qbs
                c) load data
                d) help
              Team Attributes:
                city
                formed
                starting_qb
                backup_qb
                all_qbs
              QB attributes:
                college
                drafted
                is_starting
                team_name
              Examples:
                all_qbs team "Lions" 
                city team "Giants"
                formed team "Bears"
                college qb team "Packers"
                drafted qb team "Rams"
                starting_qb team "Chargers"
                college qb "Tom Brady"
                backup_qb team "Bears"
                is_starting qb "Patrick Mahomes"
                team_name qb "Lamar Jackson"
                drafted qb "Josh Allen" """)

#Interface to DB helper functions

def team_City(name_of_team_or_qb):
    # Run SQL query to get city of team or QB
    print(db.team_city(name_of_team_or_qb[0].title()) if db.team_city(name_of_team_or_qb[0].title()) else "Not Found")

def team_Year_Formed(name_of_team_or_qb):
    # Run SQL query to get year team formed
    print(db.team_year_formed(name_of_team_or_qb[0].title()) if db.team_year_formed(
        name_of_team_or_qb[0].title()) else "Not Found")

def team_Starting_QB(name_of_team_or_qb):
    # Run SQL query to get starting QB of team
    print(db.team_starting_QB(name_of_team_or_qb[0].title()) if db.team_starting_QB(
        name_of_team_or_qb[0].title()) else "Not Found")

def team_Backup_QB(name_of_team_or_qb):
    # Run SQL query to get backup QB of team
    if((db.team_backup_QB(name_of_team_or_qb[0].title()))):
        print(*(db.team_backup_QB(name_of_team_or_qb[0].title())))
    else:
        print("Not Found")

def team_All_QBS(name_of_team_or_qb):
    # Run SQL query to get names of both QBs
    if((db.team_all_QBs(name_of_team_or_qb[0].title()))):
        print(*(db.team_all_QBs(name_of_team_or_qb[0].title())), sep=", ")
    else:
        print("Not Found")

def qb_College(name_of_team_or_qb):
    # Run SQL query to get college of QB
    print(db.QB_college(name_of_team_or_qb[0].title()) if db.QB_college(name_of_team_or_qb[0].title()) else "Not Found")

def qb_Year_Drafted(name_of_team_or_qb):
    # Run SQL query to get year drafted of QB
    print(db.QB_year_drafted(name_of_team_or_qb[0].title()) if db.QB_year_drafted(
        name_of_team_or_qb[0].title()) else "Not Found")

def qb_Is_Starter(name_of_team_or_qb):
    # Run SQL query to get whether QB is a starter or not
    print(db.is_QB_starter(name_of_team_or_qb[0].title()) if db.is_QB_starter(
        name_of_team_or_qb[0].title()) else "Not Found")

def qb_Team_Name(name_of_team_or_qb):
    # Run SQL query to get team of QB
    print(db.QB_team_name(name_of_team_or_qb[0].title()) if db.QB_team_name(
        name_of_team_or_qb[0].title()) else "Not Found")

def all_qb_College_From_Team(name_of_team_or_qb):
    # Run SQL query to get colleges of both QBs on team
    if(db.all_collage_qbs_of_team(name_of_team_or_qb[0].title())):
        print(*(db.all_collage_qbs_of_team(name_of_team_or_qb[0].title())), sep=", ")
    else:
        print("Not Found")

def all_qb_Year_Drafted_From_Team(name_of_team_or_qb):
    # Run SQL query to get year drafted of both QBs on team
    if(db.all_drafted_year_of_qbs_of_team(name_of_team_or_qb[0].title())):
        print(*(db.all_drafted_year_of_qbs_of_team(name_of_team_or_qb[0].title())), sep=", ")
    else:
        print("Not Found")

def all_qb_Is_Starter_From_Team(name_of_team_or_qb):
    # Run SQL query to get starting status of both QBs
    print(db.all_qbs_is_starter_of_team(name_of_team_or_qb[0].title()) if db.all_qbs_is_starter_of_team(
        name_of_team_or_qb[0].title()) else "Not Found")

def num_Teams():
    # Run SQL query to get number of teams
    print(db.num_teams())

def num_QBs():
    # Run SQL query to get number of QBs
    print(db.num_QBs())

# startup screen function
def startup():
    print(start_screen)
    help()

# Parsing function
# Nested if else statements (acting like a switch case) to call the respective functions
# containing the SQL queries to then be returned to the user.
def parse(user_input, user_input_words, name_of_team_or_qb, team_attributes, qb_attributes):
    # make sure the length of the input words is long enough to avoid index out of range
    if (len(user_input_words) < 3):
        user_input_words.append(" ")
    if (user_input_words[0] in team_attributes) and (user_input_words[1] == "team"):
        if user_input_words[0] == team_attributes[0]:
            team_City(name_of_team_or_qb)
        elif user_input_words[0] == team_attributes[1]:
            team_Year_Formed(name_of_team_or_qb)
        elif user_input_words[0] == team_attributes[2]:
            team_Starting_QB(name_of_team_or_qb)
        elif user_input_words[0] == team_attributes[3]:
            team_Backup_QB(name_of_team_or_qb)
        else:
            team_All_QBS(name_of_team_or_qb)
    elif (user_input_words[0] in qb_attributes) and (user_input_words[1] == "qb") and (user_input_words[2] != "team"):
        if user_input_words[0] == qb_attributes[0]:
            qb_College(name_of_team_or_qb)
        elif user_input_words[0] == qb_attributes[1]:
            qb_Year_Drafted(name_of_team_or_qb)
        elif user_input_words[0] == qb_attributes[2]:
            qb_Is_Starter(name_of_team_or_qb)
        else:
            qb_Team_Name(name_of_team_or_qb)
    elif (user_input_words[0] in qb_attributes) and (user_input_words[1] == "qb") and (user_input_words[2] == "team"):
        if user_input_words[0] == qb_attributes[0]:
            all_qb_College_From_Team(name_of_team_or_qb)
        elif user_input_words[0] == qb_attributes[1]:
            all_qb_Year_Drafted_From_Team(name_of_team_or_qb)
        elif user_input_words[0] == qb_attributes[2]:
            all_qb_Is_Starter_From_Team(name_of_team_or_qb)
        else:
            print(
                "While this query is technically in the language, it is redundant. You have asked for the team name of the qbs on a team where you input the team name.")
    elif user_input == "number of teams":
        num_Teams()
    elif user_input == "number of qbs":
        num_QBs()
    else:
        print("Your query was not in the language. Use the help function for examples and format of the language.\nCommon mistake: Don't forget to put the name of the qb or team in double quotes!")
