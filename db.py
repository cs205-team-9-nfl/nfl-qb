import sqlite3
from sqlite3 import Error
import consts
import csv


class DB:
    def __init__(self):
        self.conn = None

    def isConnected(self):
        """
        Check if the connection is established
        :return: the result boolean
        """
        if self.conn is None:
            return False
        try:
            self.conn.cursor()
            return True
        except Exception as ex:
            return False

    def isDataReady(self):
        """
        Check if the database file exists (is connected or not)
        Then check if there has table and records in the database for team and qb
        :return: the result boolean
        """
        if not self.isConnected():
            return False
        else:
            return (self.is_table_exist(consts.TABLE_TEAM_NAME) and self.is_records_exists(consts.TABLE_TEAM_NAME)) and (self.is_table_exist(consts.TABLE_QB_NAME) and self.is_records_exists(consts.TABLE_QB_NAME))

    def is_table_exist(self, table_name):
        """
        Check if the given table is in the database
        :param table_name: the target table name
        :return: the result boolean
        """
        c = self.conn.cursor()
        c.execute(consts.SQL_CHECK_TABLE_EXIST.format(TABLE_NAME=table_name))
        self.conn.commit()
        if c.fetchone()[0] == 1:
            return True
        else:
            return False

    def is_records_exists(self, table_name):
        """
        Check if the given table have the records inside
        :param table_name: the target table
        :return: result boolean
        """
        if not self.is_table_exist(table_name):
            return False
        c = self.conn.cursor()
        c.execute(consts.SQL_CHECK_RECORDS_EXIST.format(TABLE_NAME=table_name))
        self.conn.commit()
        if c.fetchone()[0]:
            return True
        else:
            return False

    def loadData(self, file_name_team=consts.CSV_TEAM, file_name_qb=consts.CSV_QB):
        """
        Load table
        :param file_name_team: const
        :param file_name_qb: const
        :return: None
        """
        try:
            if file_name_team is not None:
                if not self.is_table_exist(consts.TABLE_TEAM_NAME):
                    self.execute_query(consts.CREATE_NFL_TEAMS)
                    self.insert_nfl_teams(file_name_team)
                else:
                    if not self.is_records_exists(consts.TABLE_TEAM_NAME):
                        self.insert_nfl_teams(file_name_team)

            if file_name_qb is not None:
                if not self.is_table_exist(consts.TABLE_QB_NAME):
                    self.execute_query(consts.CREATE_NFL_QBS)
                    self.insert_nfl_qbs(file_name_qb)
                else:
                    if not self.is_records_exists(consts.TABLE_QB_NAME):
                        self.insert_nfl_qbs(file_name_qb)
        except Error:
            return False

    def count(self, table_name):
        return

    def connect_db(self):
        try:
            self.conn = sqlite3.connect(consts.DB_FILE)
            return True
        except Error as e:
            print(f'Error When init db:\n{e}')
            self.conn.close()
            return False

    def execute_query(self, query: str):
        if self.isConnected():
            try:
                c = self.conn.cursor()
                c.execute(query)
                self.conn.commit()
                return c
            except Exception as ex:
                print(f'[Incorrect SQL Statement]> {query}')
                return None
        else:
            print('Database is not connected!')
            return None

    def setup(self):
        """
        Init the database, if no file locally, create one.
        :return:
        """
        try:
            self.connect_db()
        except sqlite3.Error as e:
            print("An error has occurred. Please report this incident.")
            print(e)

    def insert_nfl_teams(self, filename):
        with open(filename, 'r') as f:
            data = csv.DictReader(f)
            for row in data:
                query = consts.INSERT_NFL_TEAMS.format(
                    name=row.get('name'),
                    city=row.get('city'),
                    year_formed=row.get('year_formed'),
                    starting_qb_name=row.get('starting_qb_name')
                )
                send = self.execute_query(query)

    def insert_nfl_qbs(self, filename):
        with open(filename, 'r') as f:
            data = csv.DictReader(f)
            for row in data:
                query = consts.INSERT_NFL_QBS.format(
                    name=row.get('name'),
                    college=row.get('college'),
                    year_drafted=row.get('year_drafted'),
                    is_starter=row.get('is_starter'),
                    team_name=row.get('team_name')
                )
                send = self.execute_query(query)

    def finish(self):
        """
        Finalizer the database
        :return:
        """
        self.conn.close()


def team_starting_QB(team: str):
    # Gets the name of the starting Qb from team and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_TEAM_NAME, fields=['starting_qb_name'], targets={'name': team})).fetchone()
    return res[0] if res is not None else False

def team_backup_QB(team: str):
    # Gets the name of the starting Qb from team and returns it
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['name'], targets={'name': team}, targets_join={'is_starter': "no"}, on={'name': 'team_name'})).fetchall()
    return [i[0] for i in res] if res is not None else False

def all_collage_qbs_of_team(team: str):
    # Gets the name of the starting Qb from team and returns it
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['name','college'], targets={'name': team}, on={'name': 'team_name'})).fetchall()
    return [{i[0]:i[1]} for i in res] if res is not None else False

def all_drafted_year_of_qbs_of_team(team: str):
    # Gets the year drafted from all the Qbs from team and returns them
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['name','year_drafted'], targets={'name': team}, on={'name': 'team_name'})).fetchall()
    return [{i[0]: i[1]} for i in res] if res is not None else False

def all_qbs_is_starter_of_team(team: str):
    # Gets the name of the starting Qb from team and returns it
    res = instance.execute_query(
        consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['name', 'is_starter'], targets={'team_name': team})).fetchall()
    return [{i[0]: i[1]} for i in res] if res is not None else False

def team_year_formed(team: str):
    # Gets the year the team was formed and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_TEAM_NAME, fields=['year_formed'], targets={'name': team})).fetchone()
    return res[0] if res is not None else False


def team_city(team: str):
    # Gets the city the team is located and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_TEAM_NAME, fields=['city'], targets={'name': team})).fetchone()
    return res[0] if res is not None else False


def team_all_QBs(team: str):
    # Gets all the names of the QBs on the team and returns them
    res = instance.execute_query(consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME, fields_join=['name'], targets={'name': team}, on={'name': 'team_name'})).fetchall()
    return [i[0] for i in res] if res is not None else False


def starting_QB_college(team: str):
    # Gets college of starting QB for team and returns it
    res = instance.execute_query(consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME, fields_join=['college'], targets={'name': team}, on={'name': 'team_name'})).fetchone()
    return res[0] if res is not None else False


def starting_QB_year_drafted(team: str):
    # GGets the year drafted of the teams starting qb and returns it
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['year_drafted'], targets={'name': team}, targets_join={'is_starter': "yes"}, on={'name': 'team_name'})).fetchone()
    return res[0] if res is not None else False


def backup_QB_college(team: str):
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['college'], targets={'name': team}, targets_join={'is_starter': "no"},
                                  on={'name': 'team_name'})).fetchone()
    return res[0] if res is not None else False


def backup_QB_Year_Drafted(team: str):
    # Gets the year drafted of the teams backup qb and returns it
    res = instance.execute_query(
        consts.get_join_statement(table_name=consts.TABLE_TEAM_NAME, table_name_join=consts.TABLE_QB_NAME,
                                  fields_join=['year_drafted'], targets={'name': team}, targets_join={'is_starter': "no"},
                                  on={'name': 'team_name'})).fetchone()
    return res[0] if res is not None else False


def QB_college(qb: str):
    # Gets the college of the qb and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['college'], targets={'name': qb})).fetchone()
    return res[0] if res is not None else False


def QB_year_drafted(qb: str):
    # Gets the year the qb was drafted and returns it
    res = instance.execute_query(
        consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['year_drafted'], targets={'name': qb})).fetchone()
    return res[0] if res is not None else False


def is_QB_starter(qb: str):
    # Gets if the qb if a starter and returns it
    res = instance.execute_query(
        consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['is_starter'], targets={'name': qb})).fetchone()
    return res[0] if res is not None else False


def QB_team_name(qb: str):
    # Gets the team name of the qb and returns it
    res = instance.execute_query(
        consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['team_name'], targets={'name': qb})).fetchone()
    return res[0] if res is not None else False


def num_teams():
    # Gets number of teams and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_TEAM_NAME, fields=['count(*)'])).fetchone()
    return res[0] if res is not None else 0


def num_QBs():
    # Gets number of QBs and returns it
    res = instance.execute_query(consts.get_query_statement(table_name=consts.TABLE_QB_NAME, fields=['count(*)'])).fetchone()
    return res[0] if res is not None else 0

#instance of db to be used in interface.py
instance = DB()