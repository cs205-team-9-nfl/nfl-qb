DB_FILE = 'test.db'

CSV_TEAM = 'data_nfl_teams.csv'
TABLE_TEAM_NAME = 'nfl_teams'
TABLE_QB_NAME = 'nfl_qbs'
CSV_QB = 'data_nfl_qbs.csv'

# init part
CREATE_NFL_TEAMS = """CREATE TABLE IF NOT EXISTS nfl_teams (
    id integer PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL,
    city text NOT NULL,
    year_formed integer NOT NULL,
    starting_qb_name text NOT NULL,
    FOREIGN KEY(starting_qb_name) REFERENCES nfl_qbs(team_name)
);"""

CREATE_NFL_QBS = """CREATE TABLE IF NOT EXISTS nfl_qbs (
    id integer PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL,
    college text NOT NULL,
    year_drafted integer NOT NULL,
    is_starter text NOT NULL,
    team_name text NOT NULL,
    FOREIGN KEY(team_name) REFERENCES nfl_teams(starting_qb_name)
);"""

INSERT_NFL_QBS = """INSERT INTO nfl_qbs
    (name, college, year_drafted, is_starter, team_name)
    VALUES("{name}", "{college}", "{year_drafted}", 
    "{is_starter}", "{team_name}");"""

INSERT_NFL_TEAMS = """INSERT INTO nfl_teams
    (name, city, year_formed, starting_qb_name)
    VALUES("{name}", "{city}", "{year_formed}", "{starting_qb_name}");
    """

SQL_CHECK_TABLE_EXIST = """SELECT count(*) FROM sqlite_master WHERE type='table' AND name="{TABLE_NAME}";"""
SQL_CHECK_RECORDS_EXIST = """SELECT count(*) FROM "{TABLE_NAME}";"""


# query part
def get_query_statement(table_name: str, fields: list = None, targets: dict = None):
    """
    A generator to give the sql statements give the params
    :param table_name: the table of the database
    :param fields: the fields want to fetch
    :param targets: the conditions when fetching the data " WHERE <targets>.key = <targets>.value
    :return: the sql query statement
    """
    query = f"""SELECT {'*' if fields is None else list_to_string(fields)} FROM `{table_name}`"""
    if targets is not None:
        query += " WHERE "
        count = 1
        for k, v in targets.items():
            query += f"""`{k}` = '{v}'{';' if count == len(targets) else ' AND '}"""
            count += 1
    return query


def get_join_statement(table_name: str, table_name_join: str, fields: list = [], fields_join: list = [], on: dict = None,
                       targets: dict = None, targets_join: dict = None):
    """
    Advanced generator with join functions for related database
    :param table_name: the table of the database
    :param table_name_join: the joined database table name
    :param fields: the fields want to fetch from table
    :param fields_join: the fields want to fetch from joined table
    :param on: the condition for join statement "ON table_name.<on>.key = table_name_join.<on>.value"
    :param targets: the conditions when fetching the data " WHERE table_name.<targets>.key = <targets>.value from table
    :param targets_join: the conditions when fetching the data " WHERE table_name_join.<targets>.key = <targets>.value from joined table
    :return:
    """
    query = f"""SELECT {'' if len(fields) == 0 else list_to_string(fields, prefix=table_name) + ', '}{table_name + '.*' if len(fields_join) == 0 else list_to_string(fields_join, prefix=table_name_join)} FROM `{table_name}`"""
    if on is not None:
        query += f' JOIN {table_name_join} ON '
        count = 1
        for k, v in on.items():
            query += f"""{table_name}.{k} = {table_name_join}.{v}{'' if count == len(targets) else ', '}"""
            count += 1

    if targets is not None or targets_join is not None:
        query += " WHERE "
    if targets is not None:
        count = 1
        for k, v in targets.items():
            query += f"""`{table_name}`.`{k}` = '{v}'{'' if count == len(targets) else ' AND '}"""
            count += 1
        if targets_join is not None:
            query += ' AND '
            count = 1
            for k, v in targets_join.items():
                query += f"""`{table_name_join}`.`{k}` = '{v}'{';' if count == len(targets_join) else ' AND '}"""
                count += 1
        else:
            query += ';'
    return query


# utils
def list_to_string(items=None, surrounding='', prefix: str = None):
    """
    Remove the brackets of list and give the comma between elements
    :param items: the list want to convert
    :param surrounding: the symbol between elements
    :param prefix: prefix of elements: <prefix>.<item>
    :return: the string output
    """
    if items is None:
        items = []
    if len(items) == 0:
        return ""
    res = ""
    count = 1
    for i in items:
        res += str('' if prefix is None else surrounding + prefix + surrounding + '.') + surrounding + str(i) + surrounding
        if count < len(items):
            res += ', '
            count += 1
    return res