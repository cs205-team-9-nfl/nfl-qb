# CS205 GROUP 9 Warm-Up Project | NFL & QBs
## Team Member
|Name|Part|
| :---: | :---: |
|Will Anthony Grana III|Interface|
|Caleb Aidan Williams|Database|
|Oliver Steven Hamburger|Interface|
|Kamiku Xue|Database|

## Intro
Welcome to our database system! This program allows the user to load csvs with data of NFL Teams and NFL QBs into an SQL database. From here, the user utilizes our custom interface query syntax to return data about the teams in the NFL and the QBs in the NFL.
## Data
NFL Teams: team names, city where team is based, year the team was formed, the name of the starting QB, the name of the backup QB.<br />
NFL QBs: name, college the QB attended, year the QB was drafted, starting status, and the name of the team the QB plays for.
## Query Syntax
Syntax:<br />

\*attribute of a team\* team \*name of the team in double quotes\*<br />
\*attribute of a qb\* qb \*name of the qb in double quotes\*<br />
\*attribute of a qb\* qb team \*name of the team in double quotes\*<br />

Team attributes:
1. city
2. formed
3. starting_qb
4. backup_qb
5. all_qbs

QB attributes:
1. college
2. drafted
3. is_starting
4. team_name

Team names:
1. Cardinals
2. Bears
3. Packers
4. Giants
5. Lions
6. Football Team
7. Eagles
8. Steelers
9. Rams
10. 49ers
11. Browns
12. Colts
13. Cowboys
14. Chiefs
15. Chargers
16. Broncos
17. Jets
18. Patriots
19. Raiders
20. Titans
21. Bills
22. Vikings
23. Falcons
24. Dolphins
25. Saints
26. Bengals
27. Seahawks
28. Buccaneers
29. Panthers
30. Jaguars
31. Ravens
32. Texans

QB names:
1. Kyler Murray
2. Colt McCoy
3. Matt Ryan
4. Josh Rosen
5. Lamar Jackson
6. Tyler Huntley
7. Josh Allen
8. Mitchell Trubisky
9. Sam Darnold
10. P.J. Walker
11. Andy Dalton
12. Justin Fields
13. Joe Burrow
14. Brandon Allen
15. Baker Mayfield
16. Case Keenum
17. Dak Prescott
18. Cooper Rush
19. Teddy Bridgewater
20. Drew Lock
21. Jared Goff
22. David Blough
23. Aaron Rodgers
24. Jordan Love
25. Tyrod Taylor
26. Deshaun Watson
27. Carson Wentz
28. Jacob Eason
29. Trevor Lawerence
30. C.J. Beathard
31. Patrick Mahomes
32. Chad Henne
33. Derek Carr
34. Marcus Mariota
35. Justin Herbert
36. Chase Daniel
37. Matthew Stafford
38. John Wolford
39. Tua Tagovailoa
40. Jacoby Brissett
41. Kirk Cousins
42. Sean Mannion
43. Mac Jones
44. Brian Hoyer
45. Jameis Winston
46. Taysom Hill
47. Daniel Jones
48. Mike Glennon
49. Zach Wilson
50. Mike White
51. Jalen Hurts
52. Joe Flacco
53. Ben Roethlisberger
54. Mason Rudolph
55. Jimmy Garoppolo
56. Trey Lance
57. Russell Wilson
58. Geno Smith
59. Tom Brady
60. Blaine Gabbert
61. Ryan Tannehill
62. Logan Woodside
63. Ryan Fitzpatrick
64. Taylor Heinicke

## References
https://en.wikipedia.org/wiki/Lists_of_National_Football_League_team_seasons<br />
https://www.ourlads.com/nfldepthcharts/depthchartpos/QB